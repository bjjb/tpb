require "uri"
require "json"
require "http/client"
require "option_parser"

module TPB
  VERSION = "0.1.0"
  URL = ENV.fetch("TPB_API_URL") { "https://apibay.org/q.php" }
  TRACKERS = [
    "udp://tracker.coppersurfer.tk:6969/announce",
    "udp://tracker.openbittorrent.com:6969/announce",
    "udp://9.rarbg.to:2710/announce",
    "udp://9.rarbg.me:2780/announce",
    "udp://9.rarbg.to:2730/announce",
    "udp://tracker.opentrackr.org:1337",
    "udp://tracker.torrent.eu.org:451/announce",
    "udp://tracker.tiny-vps.com:6969/announce",
    "udp://open.stealth.si:80/announce",
  ]

  class Result
    include JSON::Serializable
    property id : String?
    property name : String?
    property info_hash : String?
    property leechers : String?
    property seeders : String?
    property num_files : String?
    property size : String?
    property username : String?
    property added : String?
    property status : String?
    property category : String?
    property imdb : String?

    getter xt : String? { "urn:btih:#{info_hash}" }
    getter dn : String? { URI.encode_www_form(name || "Unnamed") }
    getter tr : Array(String)? { TRACKERS }
    getter params : Array(String)? { [ "xt=#{xt}", "dn=#{dn}", *tr.map { |tr| "tr=#{tr}" } ] }
    getter uri : String? { "magnet:?#{params.join("&")}" }
  end

  def exec(args)
    return if args.empty?
    Process.exec(args.shift, args)
  end

  def search(q : String?) : Enumerable(Result)
    url = URI.parse(URL)
    url.query = URI::Params.encode({ q: q })
    response = HTTP::Client.get(url)
    ensure_ok!(response.status_code)
    Array(Result).from_json(response.body)
  end

  def parse(
    args : Array(String) = ARGV,
    stdin : IO = STDIN,
    stdout : IO = STDOUT,
    stderr : IO = STDERR,
    env : Enumerable({String, String}) = ENV,
  )
    json = false
    limit : Int32 = 1
    query : String? = nil
    results = [] of Result
    OptionParser.parse(args) do |op|
      op.on "-n", "--number INT", "limit results (-1: no limit)" do |n|
        limit = n.to_i32
      end
      op.on "-v", "--version", "print the version and exit" do
        op.before_each { |x| unexpected!(x) }
        op.unknown_args { stdout.puts "tpb v#{TPB::VERSION}" }
      end
      op.on "-j", "--json", "print JSON, rather than just the link" do
        json = true
      end
      op.on "-h", "--help", "show help and exit" do
        op.before_each { |x| unexpected!(x) }
        op.unknown_args { stdout.puts op }
      end
      op.unknown_args do |args, after|
        query = args.join(" ") unless args.empty?
        results = search(query)[...limit] unless query.nil?
        if json
          stdout.puts results.to_json
        else
          results.each { |r| stdout.puts(r.uri) }
        end
        unless after.empty?
          ENV["TPB_QUERY"] = query
          ENV["TPB_RESULTS"] = results.size.to_s
          results.each_with_index { |r, i| ENV["TPB_RESULT#{i}"] = r.uri }
          exec(after)
        end
      end
    end
  end

  def unexpected!(arg : String)
    raise OptionParser::Exception.new("Unexpected argument: #{arg}")
  end

  def ensure_ok!(status : Int32)
    raise Exception.new("Unexpected status: #{status}") unless status == 200
  end

  extend self
end
