require "./tpb.cr"

begin
  TPB.parse
rescue e : Exception
  e.to_s(STDERR) unless ENV["DEBUG"]?
  e.inspect_with_backtrace(STDERR) if ENV["DEBUG"]?
  exit 1
end
