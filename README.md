# tpb

A little CLI to fetch data from apibay.org.

## Installation

You can build it if you have Crystal with

	shards build

Or check for releases.

## Usage

Use `tpb -h` for usage details.

## Development

To work on it, you'll need Crystal (check the shards.yml for the version).

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request

## Contributors

- [bjjb](https://bjjb.gitlab.io/) - creator and maintainer
